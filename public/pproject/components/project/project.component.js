(function() {

  'use strict';

  angular
    .module('pProject')

    .component('projectComponent', {
      templateUrl: 'pproject/components/project/project.component.html',
      controller: ['$scope', 'toasty', ProjectComponentController],
      bindings: {}
    });

  function ProjectComponentController($scope, toasty) {
    // ProjectComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'ProjectComponentController:',
        msg: 'started!'
      });
    };
  }
})();
