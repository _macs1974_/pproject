const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const levelSchema = new Schema({
    _id: Schema.Types.ObjectId,
    code: Number,
    description: String
});
const Level = mongoose.model('Level', levelSchema);

module.exports = Level;