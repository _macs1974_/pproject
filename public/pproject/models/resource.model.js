(function() {

  'use strict';

  angular
    .module("pProject")
    .factory("ResourceModel", ResourceModel);

  ResourceModel.$inject = ["$http", "SkillsModel"];

  function ResourceModel($http, SkillsModel) {

    var Resource = function(data) {

      this.resourceID = data.resourceID;
      this.name = data.name;
      this.surname = data.surname;
      this.skills = SkillsModel.build(data.skill, this.level);

    };

    Resource.build = function(data) {
      return new Resource(data);
    };

    Resource.save = function() {
      if (this.resourceID) {
        return $http.put(APIURL, this.data)
      }
    };

    return Resource;
  }
})();
