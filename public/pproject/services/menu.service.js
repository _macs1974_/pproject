(function() {

  'use strict';

  angular
    .module('pProject')
    .factory('MenuService', MenuService);

  function MenuService() {

    var menu = {
      isOpen: false
    };

    return {
      getMenuInstance: function() {
        return menu;
      },
      openMenu: function() {
        menu.isOpen = true;
      },
      closeMenu: function() {
        menu.isOpen = false;
      },
      toggleMenu: function() {
        menu.isOpen = !menu.isOpen;
      }

    };
  }
})();
