const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const competenceSchema = new Schema({
    _id: Schema.Types.ObjectId,
    code: Number,
    description: String,
    levels: [{
        type: Schema.Types.ObjectId,
        ref: 'Level'
    }]
});
const Competence = mongoose.model('Competence', competenceSchema);


module.exports = Competence;