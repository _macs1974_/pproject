(function() {

  'use strict';

  angular
    .module("pProject")
    .factory("SkillModel", SkillModel);

  SkillModel.$inject = ["$http", "LevelModel"];

  function SkillModel($http, LevelModel) {

    var Skill = function(data) {

      this.skillID = data.skillID;
      this.description = data.description;
      this.level = LevelModel.build(this.level);

    };

    Skill.build = function(data) {
      return new Skill(data);
    };

    Skill.save = function() {
      if (this.skillID) {
        return $http.put(APIURL, this.data)
      }
    };

    return Skill;
  }
})();
