(function() {

  'use strict';

  angular
    .module('pProject')

    .component('menuComponent', {
      templateUrl: 'pproject/components/menu/menu.component.html',
      controller: ['$scope', 'toasty', '$state', 'MenuService', '$timeout', MenuComponentController],
      bindings: {}
    });

  function MenuComponentController($scope, toasty, $state, MenuService, $timeout) {
    // MenuComponentController for PriorityProjectApp component
    var $ctrl = this;
    $ctrl.menuElements = [];
    $ctrl.menu;

    $ctrl.$onInit = function() {
      $ctrl.hamburger = false;
      $ctrl.menu = MenuService.getMenuInstance();

      $state.get().forEach(function(i, x) {
        if (i.name !== "" && i.name !== 'undefined') {
          $ctrl.menuElements.push({
            label: i.label,
            sref: i.name,
            active: ($state.current.name == i.name) ? true : false
          });
        }
      });

      toasty.success({
        timeout: 3500,
        title: 'MenuComponentController:',
        msg: 'started!'
      });
    };


    $scope.$watch(angular.bind(this, function(isOpen) {
      return this.menu.isOpen;
    }), function(newVal, oldVal) {
      if (newVal) {
        $ctrl.hamburger = true;
      } else {
        $ctrl.hamburger = false;
      }
    });

    $ctrl.openMenu = function() {
      MenuService.toggleMenu();
    };

    $ctrl.$postLink = function() {};
  }
})();
