(function() {

  'use strict';

  angular
    .module('pProject')

    .component('dashboardComponent', {
      templateUrl: 'pproject/components/dashboard/dashboard.component.html',
      controller: ['$scope', 'toasty', DashboardComponentController],
      bindings: {}
    });

  function DashboardComponentController($scope, toasty) {
    // DashboardComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'DashboardComponentController:',
        msg: 'started!'
      });
    };
  }
})();
