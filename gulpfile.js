var gulp = require('gulp');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var nodemon = require('gulp-nodemon');
var sass = require('gulp-sass');
sass.compiler = require('node-sass');

gulp.task('compressScripts', async function() {
  gulp.src('public/pproject/**/*.js')
    .pipe(sourcemaps.init({
      includeContent: false,
      sourceRoot: './'
    }))
    //.pipe(uglify())
    .pipe(sourcemaps.write())
    .pipe(concat('all.js'))
    .pipe(gulp.dest('./public/dist/'))
});

gulp.task('scss', async function () {
  return gulp.src('./public/stylesheets/scss/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./public/stylesheets/css'));
});

gulp.task('nodemon', async function() {
  nodemon({
      script: './bin/www',
      ext: 'js scss',
      ignore: ['public/dist', 'public/components', 'public/stylesheets/css', 'package-lock.json', 'gulpfile.js']
    })
    .on('restart', ['compressScripts', 'scss'])
});

// gulp.task('sass:watch', function () {
//   gulp.watch('./public/stylesheets/scss/*.scss', ['scss']);
// });
