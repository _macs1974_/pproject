(function() {
  'use strict';
  angular
    .module('pProject', [
      'gridster',
      'ui.router',
      'angular-toasty',
      'ngAnimate'
    ])
    .config(config)
    .run(run);

  config.$inject = ['$stateProvider', '$urlRouterProvider', 'toastyConfigProvider'];


  function config($stateProvider, $urlRouterProvider, toastyConfigProvider) {

    toastyConfigProvider.setConfig({
      showClose: true,
      clickToClose: true,
      timeout: 5000,
      sound: false,
      html: false,
      shake: false,
      theme: "material" // material, default
    });

    $stateProvider
      .state('index', {
        url: '/index',
        label: 'Home',
        template: '<index-component></index-component>',
        defaultActive: true
      })
      .state('index.resources', {
        url: '^/resources',
        label: 'Risorse',
        template: '<resources-container-component></resources-container-component>',
        defaultActive: false
      })
      .state('index.levels', {
        url: '^/levels',
        label: 'Livelli',
        template: '<levels-component></levels-component>',
        defaultActive: false
      })
      .state('index.skills', {
        url: '^/skills',
        label: 'Competenze',
        template: '<skills-component></skills-component>',
        defaultActive: false
      });

    $urlRouterProvider.otherwise('/index');
  }

  function run($rootScope, $timeout) {
    // $rootScope.$on('$viewContentLoaded', () => {
    //   $timeout(() => {
    //     componentHandler.upgradeAllRegistered();
    //   })
    // })
  }
})();
