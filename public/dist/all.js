(function() {
  'use strict';
  angular
    .module('pProject', [
      'gridster',
      'ui.router',
      'angular-toasty',
      'ngAnimate'
    ])
    .config(config)
    .run(run);

  config.$inject = ['$stateProvider', '$urlRouterProvider', 'toastyConfigProvider'];


  function config($stateProvider, $urlRouterProvider, toastyConfigProvider) {

    toastyConfigProvider.setConfig({
      showClose: true,
      clickToClose: true,
      timeout: 5000,
      sound: false,
      html: false,
      shake: false,
      theme: "material" // material, default
    });

    $stateProvider
      .state('index', {
        url: '/index',
        label: 'Home',
        template: '<index-component></index-component>',
        defaultActive: true
      })
      .state('index.resources', {
        url: '^/resources',
        label: 'Risorse',
        template: '<resources-container-component></resources-container-component>',
        defaultActive: false
      })
      .state('index.levels', {
        url: '^/levels',
        label: 'Livelli',
        template: '<levels-component></levels-component>',
        defaultActive: false
      })
      .state('index.skills', {
        url: '^/skills',
        label: 'Competenze',
        template: '<skills-component></skills-component>',
        defaultActive: false
      });

    $urlRouterProvider.otherwise('/index');
  }

  function run($rootScope, $timeout) {
    // $rootScope.$on('$viewContentLoaded', () => {
    //   $timeout(() => {
    //     componentHandler.upgradeAllRegistered();
    //   })
    // })
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJwcHJvamVjdC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKSB7XHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoJ3BQcm9qZWN0JywgW1xyXG4gICAgICAnZ3JpZHN0ZXInLFxyXG4gICAgICAndWkucm91dGVyJyxcclxuICAgICAgJ2FuZ3VsYXItdG9hc3R5JyxcclxuICAgICAgJ25nQW5pbWF0ZSdcclxuICAgIF0pXHJcbiAgICAuY29uZmlnKGNvbmZpZylcclxuICAgIC5ydW4ocnVuKTtcclxuXHJcbiAgY29uZmlnLiRpbmplY3QgPSBbJyRzdGF0ZVByb3ZpZGVyJywgJyR1cmxSb3V0ZXJQcm92aWRlcicsICd0b2FzdHlDb25maWdQcm92aWRlciddO1xyXG5cclxuXHJcbiAgZnVuY3Rpb24gY29uZmlnKCRzdGF0ZVByb3ZpZGVyLCAkdXJsUm91dGVyUHJvdmlkZXIsIHRvYXN0eUNvbmZpZ1Byb3ZpZGVyKSB7XHJcblxyXG4gICAgdG9hc3R5Q29uZmlnUHJvdmlkZXIuc2V0Q29uZmlnKHtcclxuICAgICAgc2hvd0Nsb3NlOiB0cnVlLFxyXG4gICAgICBjbGlja1RvQ2xvc2U6IHRydWUsXHJcbiAgICAgIHRpbWVvdXQ6IDUwMDAsXHJcbiAgICAgIHNvdW5kOiBmYWxzZSxcclxuICAgICAgaHRtbDogZmFsc2UsXHJcbiAgICAgIHNoYWtlOiBmYWxzZSxcclxuICAgICAgdGhlbWU6IFwibWF0ZXJpYWxcIiAvLyBtYXRlcmlhbCwgZGVmYXVsdFxyXG4gICAgfSk7XHJcblxyXG4gICAgJHN0YXRlUHJvdmlkZXJcclxuICAgICAgLnN0YXRlKCdpbmRleCcsIHtcclxuICAgICAgICB1cmw6ICcvaW5kZXgnLFxyXG4gICAgICAgIGxhYmVsOiAnSG9tZScsXHJcbiAgICAgICAgdGVtcGxhdGU6ICc8aW5kZXgtY29tcG9uZW50PjwvaW5kZXgtY29tcG9uZW50PicsXHJcbiAgICAgICAgZGVmYXVsdEFjdGl2ZTogdHJ1ZVxyXG4gICAgICB9KVxyXG4gICAgICAuc3RhdGUoJ2luZGV4LnJlc291cmNlcycsIHtcclxuICAgICAgICB1cmw6ICdeL3Jlc291cmNlcycsXHJcbiAgICAgICAgbGFiZWw6ICdSaXNvcnNlJyxcclxuICAgICAgICB0ZW1wbGF0ZTogJzxyZXNvdXJjZXMtY29udGFpbmVyLWNvbXBvbmVudD48L3Jlc291cmNlcy1jb250YWluZXItY29tcG9uZW50PicsXHJcbiAgICAgICAgZGVmYXVsdEFjdGl2ZTogZmFsc2VcclxuICAgICAgfSlcclxuICAgICAgLnN0YXRlKCdpbmRleC5sZXZlbHMnLCB7XHJcbiAgICAgICAgdXJsOiAnXi9sZXZlbHMnLFxyXG4gICAgICAgIGxhYmVsOiAnTGl2ZWxsaScsXHJcbiAgICAgICAgdGVtcGxhdGU6ICc8bGV2ZWxzLWNvbXBvbmVudD48L2xldmVscy1jb21wb25lbnQ+JyxcclxuICAgICAgICBkZWZhdWx0QWN0aXZlOiBmYWxzZVxyXG4gICAgICB9KVxyXG4gICAgICAuc3RhdGUoJ2luZGV4LnNraWxscycsIHtcclxuICAgICAgICB1cmw6ICdeL3NraWxscycsXHJcbiAgICAgICAgbGFiZWw6ICdDb21wZXRlbnplJyxcclxuICAgICAgICB0ZW1wbGF0ZTogJzxza2lsbHMtY29tcG9uZW50Pjwvc2tpbGxzLWNvbXBvbmVudD4nLFxyXG4gICAgICAgIGRlZmF1bHRBY3RpdmU6IGZhbHNlXHJcbiAgICAgIH0pO1xyXG5cclxuICAgICR1cmxSb3V0ZXJQcm92aWRlci5vdGhlcndpc2UoJy9pbmRleCcpO1xyXG4gIH1cclxuXHJcbiAgZnVuY3Rpb24gcnVuKCRyb290U2NvcGUsICR0aW1lb3V0KSB7XHJcbiAgICAvLyAkcm9vdFNjb3BlLiRvbignJHZpZXdDb250ZW50TG9hZGVkJywgKCkgPT4ge1xyXG4gICAgLy8gICAkdGltZW91dCgoKSA9PiB7XHJcbiAgICAvLyAgICAgY29tcG9uZW50SGFuZGxlci51cGdyYWRlQWxsUmVnaXN0ZXJlZCgpO1xyXG4gICAgLy8gICB9KVxyXG4gICAgLy8gfSlcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoicHByb2plY3QuanMifQ==

(function() {

  'use strict';

  angular
    .module("pProject")
    .factory("LevelModel", LevelModel);

  LevelModel.$inject = ["$http"];

  function LevelModel($http) {

    var Level = function(data) {

      this.levelID = data.levelID;
      this.description = data.description;

    };

    Level.build = function(data) {
      return new Skill(data);
    };

    Level.save = function() {
      if (this.levelID) {
        return $http.put(APIURL, this.data);
      }
    };

    return Level;
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtb2RlbHMvbGV2ZWwubW9kZWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoXCJwUHJvamVjdFwiKVxyXG4gICAgLmZhY3RvcnkoXCJMZXZlbE1vZGVsXCIsIExldmVsTW9kZWwpO1xyXG5cclxuICBMZXZlbE1vZGVsLiRpbmplY3QgPSBbXCIkaHR0cFwiXTtcclxuXHJcbiAgZnVuY3Rpb24gTGV2ZWxNb2RlbCgkaHR0cCkge1xyXG5cclxuICAgIHZhciBMZXZlbCA9IGZ1bmN0aW9uKGRhdGEpIHtcclxuXHJcbiAgICAgIHRoaXMubGV2ZWxJRCA9IGRhdGEubGV2ZWxJRDtcclxuICAgICAgdGhpcy5kZXNjcmlwdGlvbiA9IGRhdGEuZGVzY3JpcHRpb247XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBMZXZlbC5idWlsZCA9IGZ1bmN0aW9uKGRhdGEpIHtcclxuICAgICAgcmV0dXJuIG5ldyBTa2lsbChkYXRhKTtcclxuICAgIH07XHJcblxyXG4gICAgTGV2ZWwuc2F2ZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBpZiAodGhpcy5sZXZlbElEKSB7XHJcbiAgICAgICAgcmV0dXJuICRodHRwLnB1dChBUElVUkwsIHRoaXMuZGF0YSk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgcmV0dXJuIExldmVsO1xyXG4gIH1cclxufSkoKTtcclxuIl0sImZpbGUiOiJtb2RlbHMvbGV2ZWwubW9kZWwuanMifQ==

(function() {

  'use strict';

  angular
    .module("pProject")
    .factory("ResourceModel", ResourceModel);

  ResourceModel.$inject = ["$http", "SkillsModel"];

  function ResourceModel($http, SkillsModel) {

    var Resource = function(data) {

      this.resourceID = data.resourceID;
      this.name = data.name;
      this.surname = data.surname;
      this.skills = SkillsModel.build(data.skill, this.level);

    };

    Resource.build = function(data) {
      return new Resource(data);
    };

    Resource.save = function() {
      if (this.resourceID) {
        return $http.put(APIURL, this.data)
      }
    };

    return Resource;
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtb2RlbHMvcmVzb3VyY2UubW9kZWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoXCJwUHJvamVjdFwiKVxyXG4gICAgLmZhY3RvcnkoXCJSZXNvdXJjZU1vZGVsXCIsIFJlc291cmNlTW9kZWwpO1xyXG5cclxuICBSZXNvdXJjZU1vZGVsLiRpbmplY3QgPSBbXCIkaHR0cFwiLCBcIlNraWxsc01vZGVsXCJdO1xyXG5cclxuICBmdW5jdGlvbiBSZXNvdXJjZU1vZGVsKCRodHRwLCBTa2lsbHNNb2RlbCkge1xyXG5cclxuICAgIHZhciBSZXNvdXJjZSA9IGZ1bmN0aW9uKGRhdGEpIHtcclxuXHJcbiAgICAgIHRoaXMucmVzb3VyY2VJRCA9IGRhdGEucmVzb3VyY2VJRDtcclxuICAgICAgdGhpcy5uYW1lID0gZGF0YS5uYW1lO1xyXG4gICAgICB0aGlzLnN1cm5hbWUgPSBkYXRhLnN1cm5hbWU7XHJcbiAgICAgIHRoaXMuc2tpbGxzID0gU2tpbGxzTW9kZWwuYnVpbGQoZGF0YS5za2lsbCwgdGhpcy5sZXZlbCk7XHJcblxyXG4gICAgfTtcclxuXHJcbiAgICBSZXNvdXJjZS5idWlsZCA9IGZ1bmN0aW9uKGRhdGEpIHtcclxuICAgICAgcmV0dXJuIG5ldyBSZXNvdXJjZShkYXRhKTtcclxuICAgIH07XHJcblxyXG4gICAgUmVzb3VyY2Uuc2F2ZSA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICBpZiAodGhpcy5yZXNvdXJjZUlEKSB7XHJcbiAgICAgICAgcmV0dXJuICRodHRwLnB1dChBUElVUkwsIHRoaXMuZGF0YSlcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICByZXR1cm4gUmVzb3VyY2U7XHJcbiAgfVxyXG59KSgpO1xyXG4iXSwiZmlsZSI6Im1vZGVscy9yZXNvdXJjZS5tb2RlbC5qcyJ9

(function() {

  'use strict';

  angular
    .module("pProject")
    .factory("SkillModel", SkillModel);

  SkillModel.$inject = ["$http", "LevelModel"];

  function SkillModel($http, LevelModel) {

    var Skill = function(data) {

      this.skillID = data.skillID;
      this.description = data.description;
      this.level = LevelModel.build(this.level);

    };

    Skill.build = function(data) {
      return new Skill(data);
    };

    Skill.save = function() {
      if (this.skillID) {
        return $http.put(APIURL, this.data)
      }
    };

    return Skill;
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJtb2RlbHMvc2tpbGwubW9kZWwuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoXCJwUHJvamVjdFwiKVxyXG4gICAgLmZhY3RvcnkoXCJTa2lsbE1vZGVsXCIsIFNraWxsTW9kZWwpO1xyXG5cclxuICBTa2lsbE1vZGVsLiRpbmplY3QgPSBbXCIkaHR0cFwiLCBcIkxldmVsTW9kZWxcIl07XHJcblxyXG4gIGZ1bmN0aW9uIFNraWxsTW9kZWwoJGh0dHAsIExldmVsTW9kZWwpIHtcclxuXHJcbiAgICB2YXIgU2tpbGwgPSBmdW5jdGlvbihkYXRhKSB7XHJcblxyXG4gICAgICB0aGlzLnNraWxsSUQgPSBkYXRhLnNraWxsSUQ7XHJcbiAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBkYXRhLmRlc2NyaXB0aW9uO1xyXG4gICAgICB0aGlzLmxldmVsID0gTGV2ZWxNb2RlbC5idWlsZCh0aGlzLmxldmVsKTtcclxuXHJcbiAgICB9O1xyXG5cclxuICAgIFNraWxsLmJ1aWxkID0gZnVuY3Rpb24oZGF0YSkge1xyXG4gICAgICByZXR1cm4gbmV3IFNraWxsKGRhdGEpO1xyXG4gICAgfTtcclxuXHJcbiAgICBTa2lsbC5zYXZlID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIGlmICh0aGlzLnNraWxsSUQpIHtcclxuICAgICAgICByZXR1cm4gJGh0dHAucHV0KEFQSVVSTCwgdGhpcy5kYXRhKVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIHJldHVybiBTa2lsbDtcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoibW9kZWxzL3NraWxsLm1vZGVsLmpzIn0=

(function() {

  'use strict';

  angular
    .module('pProject')
    .factory('GridsterSetOpt', GridsterSetOpt);

  function GridsterSetOpt() {
    return {
      getOpt: function() {
        return {
          columns: 13, // the width of the grid, in columns
          pushing: true, // whether to push other items out of the way on move or resize
          floating: true, // whether to automatically float items up so they stack (you can temporarily disable if you are adding unsorted items with ng-repeat)
          swapping: true, // whether or not to have items of the same size switch places instead of pushing down if they are the same size
          width: 'auto', // can be an integer or 'auto'. 'auto' scales gridster to be the full width of its containing element
          colWidth: 'auto', // can be an integer or 'auto'.  'auto' uses the pixel width of the element divided by 'columns'
          rowHeight: 'match', // can be an integer or 'match'.  Match uses the colWidth, giving you square widgets.
          margins: [10, 10], // the pixel distance between each widget
          outerMargin: true, // whether margins apply to outer edges of the grid
          sparse: false, // "true" can increase performance of dragging and resizing for big grid (e.g. 20x50)
          isMobile: false, // stacks the grid items if true
          mobileBreakPoint: 600, // if the screen is not wider that this, remove the grid layout and stack the items
          mobileModeEnabled: true, // whether or not to toggle mobile mode when screen width is less than mobileBreakPoint
          minColumns: 1, // the minimum columns the grid must have
          minRows: 2, // the minimum height of the grid, in rows
          maxRows: 100,
          defaultSizeX: 2, // the default width of a gridster item, if not specifed
          defaultSizeY: 1, // the default height of a gridster item, if not specified
          minSizeX: 1, // minimum column width of an item
          maxSizeX: null, // maximum column width of an item
          minSizeY: 1, // minumum row height of an item
          maxSizeY: null, // maximum row height of an item
          resizable: {
            enabled: false,
            handles: ['n', 'e', 's', 'w', 'ne', 'se', 'sw', 'nw'],
            start: function(event, $element, widget) {}, // optional callback fired when resize is started,
            resize: function(event, $element, widget) {}, // optional callback fired when item is resized,
            stop: function(event, $element, widget) {} // optional callback fired when item is finished resizing
          },
          draggable: {
            enabled: false, // whether dragging items is supported
            handle: '.my-class', // optional selector for drag handle
            start: function(event, $element, widget) {}, // optional callback fired when drag is started,
            drag: function(event, $element, widget) {}, // optional callback fired when item is moved,
            stop: function(event, $element, widget) {} // optional callback fired when item is finished dragging
          }
        };
      }
    };
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzZXJ2aWNlcy9ncmlkc3Rlci5vcHRpb25zLnNlcnZpY2UuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoJ3BQcm9qZWN0JylcclxuICAgIC5mYWN0b3J5KCdHcmlkc3RlclNldE9wdCcsIEdyaWRzdGVyU2V0T3B0KTtcclxuXHJcbiAgZnVuY3Rpb24gR3JpZHN0ZXJTZXRPcHQoKSB7XHJcbiAgICByZXR1cm4ge1xyXG4gICAgICBnZXRPcHQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICBjb2x1bW5zOiAxMywgLy8gdGhlIHdpZHRoIG9mIHRoZSBncmlkLCBpbiBjb2x1bW5zXHJcbiAgICAgICAgICBwdXNoaW5nOiB0cnVlLCAvLyB3aGV0aGVyIHRvIHB1c2ggb3RoZXIgaXRlbXMgb3V0IG9mIHRoZSB3YXkgb24gbW92ZSBvciByZXNpemVcclxuICAgICAgICAgIGZsb2F0aW5nOiB0cnVlLCAvLyB3aGV0aGVyIHRvIGF1dG9tYXRpY2FsbHkgZmxvYXQgaXRlbXMgdXAgc28gdGhleSBzdGFjayAoeW91IGNhbiB0ZW1wb3JhcmlseSBkaXNhYmxlIGlmIHlvdSBhcmUgYWRkaW5nIHVuc29ydGVkIGl0ZW1zIHdpdGggbmctcmVwZWF0KVxyXG4gICAgICAgICAgc3dhcHBpbmc6IHRydWUsIC8vIHdoZXRoZXIgb3Igbm90IHRvIGhhdmUgaXRlbXMgb2YgdGhlIHNhbWUgc2l6ZSBzd2l0Y2ggcGxhY2VzIGluc3RlYWQgb2YgcHVzaGluZyBkb3duIGlmIHRoZXkgYXJlIHRoZSBzYW1lIHNpemVcclxuICAgICAgICAgIHdpZHRoOiAnYXV0bycsIC8vIGNhbiBiZSBhbiBpbnRlZ2VyIG9yICdhdXRvJy4gJ2F1dG8nIHNjYWxlcyBncmlkc3RlciB0byBiZSB0aGUgZnVsbCB3aWR0aCBvZiBpdHMgY29udGFpbmluZyBlbGVtZW50XHJcbiAgICAgICAgICBjb2xXaWR0aDogJ2F1dG8nLCAvLyBjYW4gYmUgYW4gaW50ZWdlciBvciAnYXV0bycuICAnYXV0bycgdXNlcyB0aGUgcGl4ZWwgd2lkdGggb2YgdGhlIGVsZW1lbnQgZGl2aWRlZCBieSAnY29sdW1ucydcclxuICAgICAgICAgIHJvd0hlaWdodDogJ21hdGNoJywgLy8gY2FuIGJlIGFuIGludGVnZXIgb3IgJ21hdGNoJy4gIE1hdGNoIHVzZXMgdGhlIGNvbFdpZHRoLCBnaXZpbmcgeW91IHNxdWFyZSB3aWRnZXRzLlxyXG4gICAgICAgICAgbWFyZ2luczogWzEwLCAxMF0sIC8vIHRoZSBwaXhlbCBkaXN0YW5jZSBiZXR3ZWVuIGVhY2ggd2lkZ2V0XHJcbiAgICAgICAgICBvdXRlck1hcmdpbjogdHJ1ZSwgLy8gd2hldGhlciBtYXJnaW5zIGFwcGx5IHRvIG91dGVyIGVkZ2VzIG9mIHRoZSBncmlkXHJcbiAgICAgICAgICBzcGFyc2U6IGZhbHNlLCAvLyBcInRydWVcIiBjYW4gaW5jcmVhc2UgcGVyZm9ybWFuY2Ugb2YgZHJhZ2dpbmcgYW5kIHJlc2l6aW5nIGZvciBiaWcgZ3JpZCAoZS5nLiAyMHg1MClcclxuICAgICAgICAgIGlzTW9iaWxlOiBmYWxzZSwgLy8gc3RhY2tzIHRoZSBncmlkIGl0ZW1zIGlmIHRydWVcclxuICAgICAgICAgIG1vYmlsZUJyZWFrUG9pbnQ6IDYwMCwgLy8gaWYgdGhlIHNjcmVlbiBpcyBub3Qgd2lkZXIgdGhhdCB0aGlzLCByZW1vdmUgdGhlIGdyaWQgbGF5b3V0IGFuZCBzdGFjayB0aGUgaXRlbXNcclxuICAgICAgICAgIG1vYmlsZU1vZGVFbmFibGVkOiB0cnVlLCAvLyB3aGV0aGVyIG9yIG5vdCB0byB0b2dnbGUgbW9iaWxlIG1vZGUgd2hlbiBzY3JlZW4gd2lkdGggaXMgbGVzcyB0aGFuIG1vYmlsZUJyZWFrUG9pbnRcclxuICAgICAgICAgIG1pbkNvbHVtbnM6IDEsIC8vIHRoZSBtaW5pbXVtIGNvbHVtbnMgdGhlIGdyaWQgbXVzdCBoYXZlXHJcbiAgICAgICAgICBtaW5Sb3dzOiAyLCAvLyB0aGUgbWluaW11bSBoZWlnaHQgb2YgdGhlIGdyaWQsIGluIHJvd3NcclxuICAgICAgICAgIG1heFJvd3M6IDEwMCxcclxuICAgICAgICAgIGRlZmF1bHRTaXplWDogMiwgLy8gdGhlIGRlZmF1bHQgd2lkdGggb2YgYSBncmlkc3RlciBpdGVtLCBpZiBub3Qgc3BlY2lmZWRcclxuICAgICAgICAgIGRlZmF1bHRTaXplWTogMSwgLy8gdGhlIGRlZmF1bHQgaGVpZ2h0IG9mIGEgZ3JpZHN0ZXIgaXRlbSwgaWYgbm90IHNwZWNpZmllZFxyXG4gICAgICAgICAgbWluU2l6ZVg6IDEsIC8vIG1pbmltdW0gY29sdW1uIHdpZHRoIG9mIGFuIGl0ZW1cclxuICAgICAgICAgIG1heFNpemVYOiBudWxsLCAvLyBtYXhpbXVtIGNvbHVtbiB3aWR0aCBvZiBhbiBpdGVtXHJcbiAgICAgICAgICBtaW5TaXplWTogMSwgLy8gbWludW11bSByb3cgaGVpZ2h0IG9mIGFuIGl0ZW1cclxuICAgICAgICAgIG1heFNpemVZOiBudWxsLCAvLyBtYXhpbXVtIHJvdyBoZWlnaHQgb2YgYW4gaXRlbVxyXG4gICAgICAgICAgcmVzaXphYmxlOiB7XHJcbiAgICAgICAgICAgIGVuYWJsZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBoYW5kbGVzOiBbJ24nLCAnZScsICdzJywgJ3cnLCAnbmUnLCAnc2UnLCAnc3cnLCAnbncnXSxcclxuICAgICAgICAgICAgc3RhcnQ6IGZ1bmN0aW9uKGV2ZW50LCAkZWxlbWVudCwgd2lkZ2V0KSB7fSwgLy8gb3B0aW9uYWwgY2FsbGJhY2sgZmlyZWQgd2hlbiByZXNpemUgaXMgc3RhcnRlZCxcclxuICAgICAgICAgICAgcmVzaXplOiBmdW5jdGlvbihldmVudCwgJGVsZW1lbnQsIHdpZGdldCkge30sIC8vIG9wdGlvbmFsIGNhbGxiYWNrIGZpcmVkIHdoZW4gaXRlbSBpcyByZXNpemVkLFxyXG4gICAgICAgICAgICBzdG9wOiBmdW5jdGlvbihldmVudCwgJGVsZW1lbnQsIHdpZGdldCkge30gLy8gb3B0aW9uYWwgY2FsbGJhY2sgZmlyZWQgd2hlbiBpdGVtIGlzIGZpbmlzaGVkIHJlc2l6aW5nXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgZHJhZ2dhYmxlOiB7XHJcbiAgICAgICAgICAgIGVuYWJsZWQ6IGZhbHNlLCAvLyB3aGV0aGVyIGRyYWdnaW5nIGl0ZW1zIGlzIHN1cHBvcnRlZFxyXG4gICAgICAgICAgICBoYW5kbGU6ICcubXktY2xhc3MnLCAvLyBvcHRpb25hbCBzZWxlY3RvciBmb3IgZHJhZyBoYW5kbGVcclxuICAgICAgICAgICAgc3RhcnQ6IGZ1bmN0aW9uKGV2ZW50LCAkZWxlbWVudCwgd2lkZ2V0KSB7fSwgLy8gb3B0aW9uYWwgY2FsbGJhY2sgZmlyZWQgd2hlbiBkcmFnIGlzIHN0YXJ0ZWQsXHJcbiAgICAgICAgICAgIGRyYWc6IGZ1bmN0aW9uKGV2ZW50LCAkZWxlbWVudCwgd2lkZ2V0KSB7fSwgLy8gb3B0aW9uYWwgY2FsbGJhY2sgZmlyZWQgd2hlbiBpdGVtIGlzIG1vdmVkLFxyXG4gICAgICAgICAgICBzdG9wOiBmdW5jdGlvbihldmVudCwgJGVsZW1lbnQsIHdpZGdldCkge30gLy8gb3B0aW9uYWwgY2FsbGJhY2sgZmlyZWQgd2hlbiBpdGVtIGlzIGZpbmlzaGVkIGRyYWdnaW5nXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoic2VydmljZXMvZ3JpZHN0ZXIub3B0aW9ucy5zZXJ2aWNlLmpzIn0=

(function() {

  'use strict';

  angular
    .module('pProject')
    .factory('MenuService', MenuService);

  function MenuService() {

    var menu = {
      isOpen: false
    };

    return {
      getMenuInstance: function() {
        return menu;
      },
      openMenu: function() {
        menu.isOpen = true;
      },
      closeMenu: function() {
        menu.isOpen = false;
      },
      toggleMenu: function() {
        menu.isOpen = !menu.isOpen;
      }

    };
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzZXJ2aWNlcy9tZW51LnNlcnZpY2UuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoJ3BQcm9qZWN0JylcclxuICAgIC5mYWN0b3J5KCdNZW51U2VydmljZScsIE1lbnVTZXJ2aWNlKTtcclxuXHJcbiAgZnVuY3Rpb24gTWVudVNlcnZpY2UoKSB7XHJcblxyXG4gICAgdmFyIG1lbnUgPSB7XHJcbiAgICAgIGlzT3BlbjogZmFsc2VcclxuICAgIH07XHJcblxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgZ2V0TWVudUluc3RhbmNlOiBmdW5jdGlvbigpIHtcclxuICAgICAgICByZXR1cm4gbWVudTtcclxuICAgICAgfSxcclxuICAgICAgb3Blbk1lbnU6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIG1lbnUuaXNPcGVuID0gdHJ1ZTtcclxuICAgICAgfSxcclxuICAgICAgY2xvc2VNZW51OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBtZW51LmlzT3BlbiA9IGZhbHNlO1xyXG4gICAgICB9LFxyXG4gICAgICB0b2dnbGVNZW51OiBmdW5jdGlvbigpIHtcclxuICAgICAgICBtZW51LmlzT3BlbiA9ICFtZW51LmlzT3BlbjtcclxuICAgICAgfVxyXG5cclxuICAgIH07XHJcbiAgfVxyXG59KSgpO1xyXG4iXSwiZmlsZSI6InNlcnZpY2VzL21lbnUuc2VydmljZS5qcyJ9

(function() {

  "use strict";

  angular
    .module("pProject")
    .factory("ResourceManager", ResourceManager);

  ResourceManager.$inject = ['$q', '$timeout', '$http', 'ResourceModel', 'SkillModel', 'LevelModel'];

  function ResourceManager($q, $timeout, $http, ResourceModel, SkillModel, LevelModel) {
    // factory ResourceManager for module pproject
    // create variable

    var resourceManager = {
      _pool: {},
      _retrieveInstance: function(resourceId, resourceData) {
        var instance = this._pool[resourceId];

        if (instance) {
          instance.setData(resourceData);
        } else {
          instance = new Order(resourceData);
          this._pool[resourceId] = instance;
        }
        return instance;
      },
      _search: function(resourceId) {
        return this._pool[resourceId];
      },
      _load: function(resourceId, deferred) {
        var scope = this;

        $http.get('/api/priority/get/' + resourceId)
          .success(function(resourceData) {
            var resource = scope._retrieveInstance(resourceData.id, resourceData);
            deferred.resolve(resource);
          })
          .error(function() {
            deferred.reject();
          });
      },
      /* Public Methods */
      /* Use this function in order to get a book instance by it's id */
      getResource: function(resourceId) {
        var deferred = $q.defer();
        var resource = this._search(resourceId);
        if (resource) {
          deferred.resolve(resource);
        } else {
          this._load(resourceId, deferred);
        }
        return deferred.promise;
      },
      /* Use this function in order to get instances of all the books */
      loadAllResources: function() {
        var deferred = $q.defer();
        var scope = this;
        $http.get('/api/priority/list')
          .then(function(resourcesArray) {
            var resources = [];
            resourcesArray.data.forEach(function(resourceData) {
              var resource = scope._retrieveInstance(resourceData.id, resourceData);
              resources.push(resource);
            });
            deferred.resolve(resources);
          })
          .catch(function(error) {
            deferred.reject(error);
          });
        return deferred.promise;
      },
      /*  This function is useful when we got somehow the book data and we wish to store it or update the pool and get a book instance in return */
      setResource: function(resourceData) {
        var scope = this;
        var resource = this._search(resourceData.id);
        if (resource) {
          resource.setData(resourceData);
        } else {
          resource = scope._retrieveInstance(resourceData);
        }
        return resource;
      }
    };
    return resourceManager;
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzZXJ2aWNlcy9yZXNvdXJjZS5tYW5hZ2VyLnNlcnZpY2UuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICBcInVzZSBzdHJpY3RcIjtcclxuXHJcbiAgYW5ndWxhclxyXG4gICAgLm1vZHVsZShcInBQcm9qZWN0XCIpXHJcbiAgICAuZmFjdG9yeShcIlJlc291cmNlTWFuYWdlclwiLCBSZXNvdXJjZU1hbmFnZXIpO1xyXG5cclxuICBSZXNvdXJjZU1hbmFnZXIuJGluamVjdCA9IFsnJHEnLCAnJHRpbWVvdXQnLCAnJGh0dHAnLCAnUmVzb3VyY2VNb2RlbCcsICdTa2lsbE1vZGVsJywgJ0xldmVsTW9kZWwnXTtcclxuXHJcbiAgZnVuY3Rpb24gUmVzb3VyY2VNYW5hZ2VyKCRxLCAkdGltZW91dCwgJGh0dHAsIFJlc291cmNlTW9kZWwsIFNraWxsTW9kZWwsIExldmVsTW9kZWwpIHtcclxuICAgIC8vIGZhY3RvcnkgUmVzb3VyY2VNYW5hZ2VyIGZvciBtb2R1bGUgcHByb2plY3RcclxuICAgIC8vIGNyZWF0ZSB2YXJpYWJsZVxyXG5cclxuICAgIHZhciByZXNvdXJjZU1hbmFnZXIgPSB7XHJcbiAgICAgIF9wb29sOiB7fSxcclxuICAgICAgX3JldHJpZXZlSW5zdGFuY2U6IGZ1bmN0aW9uKHJlc291cmNlSWQsIHJlc291cmNlRGF0YSkge1xyXG4gICAgICAgIHZhciBpbnN0YW5jZSA9IHRoaXMuX3Bvb2xbcmVzb3VyY2VJZF07XHJcblxyXG4gICAgICAgIGlmIChpbnN0YW5jZSkge1xyXG4gICAgICAgICAgaW5zdGFuY2Uuc2V0RGF0YShyZXNvdXJjZURhdGEpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICBpbnN0YW5jZSA9IG5ldyBPcmRlcihyZXNvdXJjZURhdGEpO1xyXG4gICAgICAgICAgdGhpcy5fcG9vbFtyZXNvdXJjZUlkXSA9IGluc3RhbmNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaW5zdGFuY2U7XHJcbiAgICAgIH0sXHJcbiAgICAgIF9zZWFyY2g6IGZ1bmN0aW9uKHJlc291cmNlSWQpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fcG9vbFtyZXNvdXJjZUlkXTtcclxuICAgICAgfSxcclxuICAgICAgX2xvYWQ6IGZ1bmN0aW9uKHJlc291cmNlSWQsIGRlZmVycmVkKSB7XHJcbiAgICAgICAgdmFyIHNjb3BlID0gdGhpcztcclxuXHJcbiAgICAgICAgJGh0dHAuZ2V0KCcvYXBpL3ByaW9yaXR5L2dldC8nICsgcmVzb3VyY2VJZClcclxuICAgICAgICAgIC5zdWNjZXNzKGZ1bmN0aW9uKHJlc291cmNlRGF0YSkge1xyXG4gICAgICAgICAgICB2YXIgcmVzb3VyY2UgPSBzY29wZS5fcmV0cmlldmVJbnN0YW5jZShyZXNvdXJjZURhdGEuaWQsIHJlc291cmNlRGF0YSk7XHJcbiAgICAgICAgICAgIGRlZmVycmVkLnJlc29sdmUocmVzb3VyY2UpO1xyXG4gICAgICAgICAgfSlcclxuICAgICAgICAgIC5lcnJvcihmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgZGVmZXJyZWQucmVqZWN0KCk7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgfSxcclxuICAgICAgLyogUHVibGljIE1ldGhvZHMgKi9cclxuICAgICAgLyogVXNlIHRoaXMgZnVuY3Rpb24gaW4gb3JkZXIgdG8gZ2V0IGEgYm9vayBpbnN0YW5jZSBieSBpdCdzIGlkICovXHJcbiAgICAgIGdldFJlc291cmNlOiBmdW5jdGlvbihyZXNvdXJjZUlkKSB7XHJcbiAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcclxuICAgICAgICB2YXIgcmVzb3VyY2UgPSB0aGlzLl9zZWFyY2gocmVzb3VyY2VJZCk7XHJcbiAgICAgICAgaWYgKHJlc291cmNlKSB7XHJcbiAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHJlc291cmNlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5fbG9hZChyZXNvdXJjZUlkLCBkZWZlcnJlZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBkZWZlcnJlZC5wcm9taXNlO1xyXG4gICAgICB9LFxyXG4gICAgICAvKiBVc2UgdGhpcyBmdW5jdGlvbiBpbiBvcmRlciB0byBnZXQgaW5zdGFuY2VzIG9mIGFsbCB0aGUgYm9va3MgKi9cclxuICAgICAgbG9hZEFsbFJlc291cmNlczogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgdmFyIGRlZmVycmVkID0gJHEuZGVmZXIoKTtcclxuICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xyXG4gICAgICAgICRodHRwLmdldCgnL2FwaS9wcmlvcml0eS9saXN0JylcclxuICAgICAgICAgIC50aGVuKGZ1bmN0aW9uKHJlc291cmNlc0FycmF5KSB7XHJcbiAgICAgICAgICAgIHZhciByZXNvdXJjZXMgPSBbXTtcclxuICAgICAgICAgICAgcmVzb3VyY2VzQXJyYXkuZGF0YS5mb3JFYWNoKGZ1bmN0aW9uKHJlc291cmNlRGF0YSkge1xyXG4gICAgICAgICAgICAgIHZhciByZXNvdXJjZSA9IHNjb3BlLl9yZXRyaWV2ZUluc3RhbmNlKHJlc291cmNlRGF0YS5pZCwgcmVzb3VyY2VEYXRhKTtcclxuICAgICAgICAgICAgICByZXNvdXJjZXMucHVzaChyZXNvdXJjZSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICBkZWZlcnJlZC5yZXNvbHZlKHJlc291cmNlcyk7XHJcbiAgICAgICAgICB9KVxyXG4gICAgICAgICAgLmNhdGNoKGZ1bmN0aW9uKGVycm9yKSB7XHJcbiAgICAgICAgICAgIGRlZmVycmVkLnJlamVjdChlcnJvcik7XHJcbiAgICAgICAgICB9KTtcclxuICAgICAgICByZXR1cm4gZGVmZXJyZWQucHJvbWlzZTtcclxuICAgICAgfSxcclxuICAgICAgLyogIFRoaXMgZnVuY3Rpb24gaXMgdXNlZnVsIHdoZW4gd2UgZ290IHNvbWVob3cgdGhlIGJvb2sgZGF0YSBhbmQgd2Ugd2lzaCB0byBzdG9yZSBpdCBvciB1cGRhdGUgdGhlIHBvb2wgYW5kIGdldCBhIGJvb2sgaW5zdGFuY2UgaW4gcmV0dXJuICovXHJcbiAgICAgIHNldFJlc291cmNlOiBmdW5jdGlvbihyZXNvdXJjZURhdGEpIHtcclxuICAgICAgICB2YXIgc2NvcGUgPSB0aGlzO1xyXG4gICAgICAgIHZhciByZXNvdXJjZSA9IHRoaXMuX3NlYXJjaChyZXNvdXJjZURhdGEuaWQpO1xyXG4gICAgICAgIGlmIChyZXNvdXJjZSkge1xyXG4gICAgICAgICAgcmVzb3VyY2Uuc2V0RGF0YShyZXNvdXJjZURhdGEpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICByZXNvdXJjZSA9IHNjb3BlLl9yZXRyaWV2ZUluc3RhbmNlKHJlc291cmNlRGF0YSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiByZXNvdXJjZTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuICAgIHJldHVybiByZXNvdXJjZU1hbmFnZXI7XHJcbiAgfVxyXG59KSgpO1xyXG4iXSwiZmlsZSI6InNlcnZpY2VzL3Jlc291cmNlLm1hbmFnZXIuc2VydmljZS5qcyJ9

(function() {

  'use strict';

  angular
    .module('pProject')

    .component('dashboardComponent', {
      templateUrl: 'pproject/components/dashboard/dashboard.component.html',
      controller: ['$scope', 'toasty', DashboardComponentController],
      bindings: {}
    });

  function DashboardComponentController($scope, toasty) {
    // DashboardComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'DashboardComponentController:',
        msg: 'started!'
      });
    };
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpIHtcclxuXHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICBhbmd1bGFyXHJcbiAgICAubW9kdWxlKCdwUHJvamVjdCcpXHJcblxyXG4gICAgLmNvbXBvbmVudCgnZGFzaGJvYXJkQ29tcG9uZW50Jywge1xyXG4gICAgICB0ZW1wbGF0ZVVybDogJ3Bwcm9qZWN0L2NvbXBvbmVudHMvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICAgIGNvbnRyb2xsZXI6IFsnJHNjb3BlJywgJ3RvYXN0eScsIERhc2hib2FyZENvbXBvbmVudENvbnRyb2xsZXJdLFxyXG4gICAgICBiaW5kaW5nczoge31cclxuICAgIH0pO1xyXG5cclxuICBmdW5jdGlvbiBEYXNoYm9hcmRDb21wb25lbnRDb250cm9sbGVyKCRzY29wZSwgdG9hc3R5KSB7XHJcbiAgICAvLyBEYXNoYm9hcmRDb21wb25lbnRDb250cm9sbGVyIGZvciBQcmlvcml0eVByb2plY3RBcHAgY29tcG9uZW50XHJcbiAgICB2YXIgJGN0cmwgPSB0aGlzO1xyXG5cclxuICAgICRjdHJsLiRvbkluaXQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdG9hc3R5LnN1Y2Nlc3Moe1xyXG4gICAgICAgIHRpbWVvdXQ6IDM1MDAsXHJcbiAgICAgICAgdGl0bGU6ICdEYXNoYm9hcmRDb21wb25lbnRDb250cm9sbGVyOicsXHJcbiAgICAgICAgbXNnOiAnc3RhcnRlZCEnXHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoiY29tcG9uZW50cy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5qcyJ9

(function() {

  'use strict';

  angular
    .module('pProject')

    .component('indexComponent', {
      templateUrl: 'pproject/components/index/index.component.html',
      controller: ['$scope', 'toasty', IndexComponentController],
      bindings: {}
    });

  function IndexComponentController($scope, toasty) {
    // IndexComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'IndexComponentController:',
        msg: 'started!'
      });
    };
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL2luZGV4L2luZGV4LmNvbXBvbmVudC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKSB7XHJcblxyXG4gICd1c2Ugc3RyaWN0JztcclxuXHJcbiAgYW5ndWxhclxyXG4gICAgLm1vZHVsZSgncFByb2plY3QnKVxyXG5cclxuICAgIC5jb21wb25lbnQoJ2luZGV4Q29tcG9uZW50Jywge1xyXG4gICAgICB0ZW1wbGF0ZVVybDogJ3Bwcm9qZWN0L2NvbXBvbmVudHMvaW5kZXgvaW5kZXguY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgICBjb250cm9sbGVyOiBbJyRzY29wZScsICd0b2FzdHknLCBJbmRleENvbXBvbmVudENvbnRyb2xsZXJdLFxyXG4gICAgICBiaW5kaW5nczoge31cclxuICAgIH0pO1xyXG5cclxuICBmdW5jdGlvbiBJbmRleENvbXBvbmVudENvbnRyb2xsZXIoJHNjb3BlLCB0b2FzdHkpIHtcclxuICAgIC8vIEluZGV4Q29tcG9uZW50Q29udHJvbGxlciBmb3IgUHJpb3JpdHlQcm9qZWN0QXBwIGNvbXBvbmVudFxyXG4gICAgdmFyICRjdHJsID0gdGhpcztcclxuXHJcbiAgICAkY3RybC4kb25Jbml0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHRvYXN0eS5zdWNjZXNzKHtcclxuICAgICAgICB0aW1lb3V0OiAzNTAwLFxyXG4gICAgICAgIHRpdGxlOiAnSW5kZXhDb21wb25lbnRDb250cm9sbGVyOicsXHJcbiAgICAgICAgbXNnOiAnc3RhcnRlZCEnXHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoiY29tcG9uZW50cy9pbmRleC9pbmRleC5jb21wb25lbnQuanMifQ==

(function() {

  'use strict';

  angular
    .module('pProject')

    .component('levelsComponent', {
      templateUrl: 'pproject/components/levels/levels.component.html',
      controller: ['$scope', 'toasty', LevelsComponentController],
      bindings: {}
    });

  function LevelsComponentController($scope, toasty) {
    // LevelsComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'LevelsComponentController:',
        msg: 'started!'
      });
    };
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL2xldmVscy9sZXZlbHMuY29tcG9uZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpIHtcclxuXHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICBhbmd1bGFyXHJcbiAgICAubW9kdWxlKCdwUHJvamVjdCcpXHJcblxyXG4gICAgLmNvbXBvbmVudCgnbGV2ZWxzQ29tcG9uZW50Jywge1xyXG4gICAgICB0ZW1wbGF0ZVVybDogJ3Bwcm9qZWN0L2NvbXBvbmVudHMvbGV2ZWxzL2xldmVscy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICAgIGNvbnRyb2xsZXI6IFsnJHNjb3BlJywgJ3RvYXN0eScsIExldmVsc0NvbXBvbmVudENvbnRyb2xsZXJdLFxyXG4gICAgICBiaW5kaW5nczoge31cclxuICAgIH0pO1xyXG5cclxuICBmdW5jdGlvbiBMZXZlbHNDb21wb25lbnRDb250cm9sbGVyKCRzY29wZSwgdG9hc3R5KSB7XHJcbiAgICAvLyBMZXZlbHNDb21wb25lbnRDb250cm9sbGVyIGZvciBQcmlvcml0eVByb2plY3RBcHAgY29tcG9uZW50XHJcbiAgICB2YXIgJGN0cmwgPSB0aGlzO1xyXG5cclxuICAgICRjdHJsLiRvbkluaXQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdG9hc3R5LnN1Y2Nlc3Moe1xyXG4gICAgICAgIHRpbWVvdXQ6IDM1MDAsXHJcbiAgICAgICAgdGl0bGU6ICdMZXZlbHNDb21wb25lbnRDb250cm9sbGVyOicsXHJcbiAgICAgICAgbXNnOiAnc3RhcnRlZCEnXHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoiY29tcG9uZW50cy9sZXZlbHMvbGV2ZWxzLmNvbXBvbmVudC5qcyJ9

(function() {

  'use strict';

  angular
    .module('pProject')

    .component('menuComponent', {
      templateUrl: 'pproject/components/menu/menu.component.html',
      controller: ['$scope', 'toasty', '$state', 'MenuService', '$timeout', MenuComponentController],
      bindings: {}
    });

  function MenuComponentController($scope, toasty, $state, MenuService, $timeout) {
    // MenuComponentController for PriorityProjectApp component
    var $ctrl = this;
    $ctrl.menuElements = [];
    $ctrl.menu;

    $ctrl.$onInit = function() {
      $ctrl.hamburger = false;
      $ctrl.menu = MenuService.getMenuInstance();

      $state.get().forEach(function(i, x) {
        if (i.name !== "" && i.name !== 'undefined') {
          $ctrl.menuElements.push({
            label: i.label,
            sref: i.name,
            active: ($state.current.name == i.name) ? true : false
          });
        }
      });

      toasty.success({
        timeout: 3500,
        title: 'MenuComponentController:',
        msg: 'started!'
      });
    };


    $scope.$watch(angular.bind(this, function(isOpen) {
      return this.menu.isOpen;
    }), function(newVal, oldVal) {
      if (newVal) {
        $ctrl.hamburger = true;
      } else {
        $ctrl.hamburger = false;
      }
    });

    $ctrl.openMenu = function() {
      MenuService.toggleMenu();
    };

    $ctrl.$postLink = function() {};
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL21lbnUvbWVudS5jb21wb25lbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoJ3BQcm9qZWN0JylcclxuXHJcbiAgICAuY29tcG9uZW50KCdtZW51Q29tcG9uZW50Jywge1xyXG4gICAgICB0ZW1wbGF0ZVVybDogJ3Bwcm9qZWN0L2NvbXBvbmVudHMvbWVudS9tZW51LmNvbXBvbmVudC5odG1sJyxcclxuICAgICAgY29udHJvbGxlcjogWyckc2NvcGUnLCAndG9hc3R5JywgJyRzdGF0ZScsICdNZW51U2VydmljZScsICckdGltZW91dCcsIE1lbnVDb21wb25lbnRDb250cm9sbGVyXSxcclxuICAgICAgYmluZGluZ3M6IHt9XHJcbiAgICB9KTtcclxuXHJcbiAgZnVuY3Rpb24gTWVudUNvbXBvbmVudENvbnRyb2xsZXIoJHNjb3BlLCB0b2FzdHksICRzdGF0ZSwgTWVudVNlcnZpY2UsICR0aW1lb3V0KSB7XHJcbiAgICAvLyBNZW51Q29tcG9uZW50Q29udHJvbGxlciBmb3IgUHJpb3JpdHlQcm9qZWN0QXBwIGNvbXBvbmVudFxyXG4gICAgdmFyICRjdHJsID0gdGhpcztcclxuICAgICRjdHJsLm1lbnVFbGVtZW50cyA9IFtdO1xyXG4gICAgJGN0cmwubWVudTtcclxuXHJcbiAgICAkY3RybC4kb25Jbml0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgICRjdHJsLmhhbWJ1cmdlciA9IGZhbHNlO1xyXG4gICAgICAkY3RybC5tZW51ID0gTWVudVNlcnZpY2UuZ2V0TWVudUluc3RhbmNlKCk7XHJcblxyXG4gICAgICAkc3RhdGUuZ2V0KCkuZm9yRWFjaChmdW5jdGlvbihpLCB4KSB7XHJcbiAgICAgICAgaWYgKGkubmFtZSAhPT0gXCJcIiAmJiBpLm5hbWUgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAkY3RybC5tZW51RWxlbWVudHMucHVzaCh7XHJcbiAgICAgICAgICAgIGxhYmVsOiBpLmxhYmVsLFxyXG4gICAgICAgICAgICBzcmVmOiBpLm5hbWUsXHJcbiAgICAgICAgICAgIGFjdGl2ZTogKCRzdGF0ZS5jdXJyZW50Lm5hbWUgPT0gaS5uYW1lKSA/IHRydWUgOiBmYWxzZVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9KTtcclxuXHJcbiAgICAgIHRvYXN0eS5zdWNjZXNzKHtcclxuICAgICAgICB0aW1lb3V0OiAzNTAwLFxyXG4gICAgICAgIHRpdGxlOiAnTWVudUNvbXBvbmVudENvbnRyb2xsZXI6JyxcclxuICAgICAgICBtc2c6ICdzdGFydGVkISdcclxuICAgICAgfSk7XHJcbiAgICB9O1xyXG5cclxuXHJcbiAgICAkc2NvcGUuJHdhdGNoKGFuZ3VsYXIuYmluZCh0aGlzLCBmdW5jdGlvbihpc09wZW4pIHtcclxuICAgICAgcmV0dXJuIHRoaXMubWVudS5pc09wZW47XHJcbiAgICB9KSwgZnVuY3Rpb24obmV3VmFsLCBvbGRWYWwpIHtcclxuICAgICAgaWYgKG5ld1ZhbCkge1xyXG4gICAgICAgICRjdHJsLmhhbWJ1cmdlciA9IHRydWU7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgJGN0cmwuaGFtYnVyZ2VyID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgICRjdHJsLm9wZW5NZW51ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIE1lbnVTZXJ2aWNlLnRvZ2dsZU1lbnUoKTtcclxuICAgIH07XHJcblxyXG4gICAgJGN0cmwuJHBvc3RMaW5rID0gZnVuY3Rpb24oKSB7fTtcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoiY29tcG9uZW50cy9tZW51L21lbnUuY29tcG9uZW50LmpzIn0=

(function() {

  'use strict';

  angular
    .module('pProject')

    .component('projectComponent', {
      templateUrl: 'pproject/components/project/project.component.html',
      controller: ['$scope', 'toasty', ProjectComponentController],
      bindings: {}
    });

  function ProjectComponentController($scope, toasty) {
    // ProjectComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'ProjectComponentController:',
        msg: 'started!'
      });
    };
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL3Byb2plY3QvcHJvamVjdC5jb21wb25lbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoJ3BQcm9qZWN0JylcclxuXHJcbiAgICAuY29tcG9uZW50KCdwcm9qZWN0Q29tcG9uZW50Jywge1xyXG4gICAgICB0ZW1wbGF0ZVVybDogJ3Bwcm9qZWN0L2NvbXBvbmVudHMvcHJvamVjdC9wcm9qZWN0LmNvbXBvbmVudC5odG1sJyxcclxuICAgICAgY29udHJvbGxlcjogWyckc2NvcGUnLCAndG9hc3R5JywgUHJvamVjdENvbXBvbmVudENvbnRyb2xsZXJdLFxyXG4gICAgICBiaW5kaW5nczoge31cclxuICAgIH0pO1xyXG5cclxuICBmdW5jdGlvbiBQcm9qZWN0Q29tcG9uZW50Q29udHJvbGxlcigkc2NvcGUsIHRvYXN0eSkge1xyXG4gICAgLy8gUHJvamVjdENvbXBvbmVudENvbnRyb2xsZXIgZm9yIFByaW9yaXR5UHJvamVjdEFwcCBjb21wb25lbnRcclxuICAgIHZhciAkY3RybCA9IHRoaXM7XHJcblxyXG4gICAgJGN0cmwuJG9uSW5pdCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICB0b2FzdHkuc3VjY2Vzcyh7XHJcbiAgICAgICAgdGltZW91dDogMzUwMCxcclxuICAgICAgICB0aXRsZTogJ1Byb2plY3RDb21wb25lbnRDb250cm9sbGVyOicsXHJcbiAgICAgICAgbXNnOiAnc3RhcnRlZCEnXHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoiY29tcG9uZW50cy9wcm9qZWN0L3Byb2plY3QuY29tcG9uZW50LmpzIn0=

(function() {

    'use strict';

    angular
        .module('pProject')

    .component('resourceComponent', {
        templateUrl: 'pproject/components/resources/resource.component.html',
        controller: ['$scope', 'toasty', '$timeout', ResourceComponentController],
        bindings: {
            userItem: '<',
            sortItemsCb: '&'
        }
    });

    function ResourceComponentController($scope, toasty, $timeout) {
        // ResourcesComponentController for PriorityProjectApp component
        var $ctrl = this;

        $ctrl.$onInit = function() {
            toasty.success({
                timeout: 3500,
                title: 'ResourceComponentController:',
                msg: 'started! ' + $ctrl.userItem.color
            });
        };

        $ctrl.addResource = function() {};

        $ctrl.showBackCard = function(item) {
            $ctrl.sortItemsCb({
                id: item.id
            });

            item.showBack = !item.showBack;
            if (item.showBack) {
                //item.sizeX = 6;
                // item.sizeY = 2;
            } else {
                //item.sizeX = 3;
                // item.sizeY = 2;
            }
        };

        $ctrl.$postLink = function() {};
    }
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL3Jlc291cmNlcy9yZXNvdXJjZS5jb21wb25lbnQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAgICd1c2Ugc3RyaWN0JztcclxuXHJcbiAgICBhbmd1bGFyXHJcbiAgICAgICAgLm1vZHVsZSgncFByb2plY3QnKVxyXG5cclxuICAgIC5jb21wb25lbnQoJ3Jlc291cmNlQ29tcG9uZW50Jywge1xyXG4gICAgICAgIHRlbXBsYXRlVXJsOiAncHByb2plY3QvY29tcG9uZW50cy9yZXNvdXJjZXMvcmVzb3VyY2UuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgICAgIGNvbnRyb2xsZXI6IFsnJHNjb3BlJywgJ3RvYXN0eScsICckdGltZW91dCcsIFJlc291cmNlQ29tcG9uZW50Q29udHJvbGxlcl0sXHJcbiAgICAgICAgYmluZGluZ3M6IHtcclxuICAgICAgICAgICAgdXNlckl0ZW06ICc8JyxcclxuICAgICAgICAgICAgc29ydEl0ZW1zQ2I6ICcmJ1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIGZ1bmN0aW9uIFJlc291cmNlQ29tcG9uZW50Q29udHJvbGxlcigkc2NvcGUsIHRvYXN0eSwgJHRpbWVvdXQpIHtcclxuICAgICAgICAvLyBSZXNvdXJjZXNDb21wb25lbnRDb250cm9sbGVyIGZvciBQcmlvcml0eVByb2plY3RBcHAgY29tcG9uZW50XHJcbiAgICAgICAgdmFyICRjdHJsID0gdGhpcztcclxuXHJcbiAgICAgICAgJGN0cmwuJG9uSW5pdCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICB0b2FzdHkuc3VjY2Vzcyh7XHJcbiAgICAgICAgICAgICAgICB0aW1lb3V0OiAzNTAwLFxyXG4gICAgICAgICAgICAgICAgdGl0bGU6ICdSZXNvdXJjZUNvbXBvbmVudENvbnRyb2xsZXI6JyxcclxuICAgICAgICAgICAgICAgIG1zZzogJ3N0YXJ0ZWQhICcgKyAkY3RybC51c2VySXRlbS5jb2xvclxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAkY3RybC5hZGRSZXNvdXJjZSA9IGZ1bmN0aW9uKCkge307XHJcblxyXG4gICAgICAgICRjdHJsLnNob3dCYWNrQ2FyZCA9IGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgICAgICAgJGN0cmwuc29ydEl0ZW1zQ2Ioe1xyXG4gICAgICAgICAgICAgICAgaWQ6IGl0ZW0uaWRcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBpdGVtLnNob3dCYWNrID0gIWl0ZW0uc2hvd0JhY2s7XHJcbiAgICAgICAgICAgIGlmIChpdGVtLnNob3dCYWNrKSB7XHJcbiAgICAgICAgICAgICAgICAvL2l0ZW0uc2l6ZVggPSA2O1xyXG4gICAgICAgICAgICAgICAgLy8gaXRlbS5zaXplWSA9IDI7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvL2l0ZW0uc2l6ZVggPSAzO1xyXG4gICAgICAgICAgICAgICAgLy8gaXRlbS5zaXplWSA9IDI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICAkY3RybC4kcG9zdExpbmsgPSBmdW5jdGlvbigpIHt9O1xyXG4gICAgfVxyXG59KSgpOyJdLCJmaWxlIjoiY29tcG9uZW50cy9yZXNvdXJjZXMvcmVzb3VyY2UuY29tcG9uZW50LmpzIn0=

(function() {

    'use strict';

    angular
        .module('pProject')
        .component('resourcesContainerComponent', {
            templateUrl: 'pproject/components/resources/resources-container.component.html',
            controller: ['$scope', 'toasty', 'GridsterSetOpt', ResourcesContainerComponentController],
            bindings: {}
        });

    function ResourcesContainerComponentController($scope, toasty, GridsterSetOpt) {
        // ResourcesComponentController for PriorityProjectApp component

        var $ctrl = this;
        $ctrl.standardItems = [];

        $ctrl.$onInit = function() {

            for (var i = 0; i < 4; i++) {
                $ctrl.standardItems.push({
                    id: i,
                    sizeX: 3,
                    sizeY: 2,
                    color: _randomColor(),
                    showBack: false,
                    userSettingsOn: false
                });
            }

            $ctrl.gridsterOpt = GridsterSetOpt.getOpt();

            // toasty.success({
            //   timeout: 3500,
            //   title: 'ResourcesComponentController:',
            //   msg: 'started!'
            // });
        };

        $ctrl.addResource = function() {
            $ctrl.addUser = !$ctrl.addUser;
            // $ctrl.standardItems.push({
            //   sizeX: 2,
            //   sizeY: 1,
            //   color: _randomColor(),
            //   userSettingsOn: false
            // });
        };

        $ctrl.openDetail = function(item) {
            $ctrl.standardItems.forEach(function(i, x) {
                if (item.color !== i.color) {
                    i.userSettingsOn = false;
                    i.sizeY = 2;
                    i.sizeX = 3;
                }
            });

            item.sizeX == 4 ? item.sizeX = 2 : item.sizeX = 4;
            item.sizeY == 2 ? item.sizeY = 1 : item.sizeY = 2;
            item.userSettingsOn == true ? item.userSettingsOn = false : item.userSettingsOn = true;
        };

        function _randomColor() {
            var c = "#";
            for (var i = 0; i < 6; i++) {
                c += (Math.random() * 16 | 0).toString(16);
            }
            return c;
        }

        $ctrl.$postLink = function() {};


        $ctrl.sortItems = function(id) {
            $ctrl.standardItems.forEach(function(i, x) {
                if (i.id != id) {
                  var lRow = parseInt(i.row) +1;
                    i.row = lRow.toString();
                    console.log(i);
                }
            });
        };
    }
})();
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL3Jlc291cmNlcy9yZXNvdXJjZXMtY29udGFpbmVyLmNvbXBvbmVudC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICAgIGFuZ3VsYXJcclxuICAgICAgICAubW9kdWxlKCdwUHJvamVjdCcpXHJcbiAgICAgICAgLmNvbXBvbmVudCgncmVzb3VyY2VzQ29udGFpbmVyQ29tcG9uZW50Jywge1xyXG4gICAgICAgICAgICB0ZW1wbGF0ZVVybDogJ3Bwcm9qZWN0L2NvbXBvbmVudHMvcmVzb3VyY2VzL3Jlc291cmNlcy1jb250YWluZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgICAgICAgICBjb250cm9sbGVyOiBbJyRzY29wZScsICd0b2FzdHknLCAnR3JpZHN0ZXJTZXRPcHQnLCBSZXNvdXJjZXNDb250YWluZXJDb21wb25lbnRDb250cm9sbGVyXSxcclxuICAgICAgICAgICAgYmluZGluZ3M6IHt9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgZnVuY3Rpb24gUmVzb3VyY2VzQ29udGFpbmVyQ29tcG9uZW50Q29udHJvbGxlcigkc2NvcGUsIHRvYXN0eSwgR3JpZHN0ZXJTZXRPcHQpIHtcclxuICAgICAgICAvLyBSZXNvdXJjZXNDb21wb25lbnRDb250cm9sbGVyIGZvciBQcmlvcml0eVByb2plY3RBcHAgY29tcG9uZW50XHJcblxyXG4gICAgICAgIHZhciAkY3RybCA9IHRoaXM7XHJcbiAgICAgICAgJGN0cmwuc3RhbmRhcmRJdGVtcyA9IFtdO1xyXG5cclxuICAgICAgICAkY3RybC4kb25Jbml0ID0gZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IDQ7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgJGN0cmwuc3RhbmRhcmRJdGVtcy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogaSxcclxuICAgICAgICAgICAgICAgICAgICBzaXplWDogMyxcclxuICAgICAgICAgICAgICAgICAgICBzaXplWTogMixcclxuICAgICAgICAgICAgICAgICAgICBjb2xvcjogX3JhbmRvbUNvbG9yKCksXHJcbiAgICAgICAgICAgICAgICAgICAgc2hvd0JhY2s6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZXJTZXR0aW5nc09uOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICRjdHJsLmdyaWRzdGVyT3B0ID0gR3JpZHN0ZXJTZXRPcHQuZ2V0T3B0KCk7XHJcblxyXG4gICAgICAgICAgICAvLyB0b2FzdHkuc3VjY2Vzcyh7XHJcbiAgICAgICAgICAgIC8vICAgdGltZW91dDogMzUwMCxcclxuICAgICAgICAgICAgLy8gICB0aXRsZTogJ1Jlc291cmNlc0NvbXBvbmVudENvbnRyb2xsZXI6JyxcclxuICAgICAgICAgICAgLy8gICBtc2c6ICdzdGFydGVkISdcclxuICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgJGN0cmwuYWRkUmVzb3VyY2UgPSBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgJGN0cmwuYWRkVXNlciA9ICEkY3RybC5hZGRVc2VyO1xyXG4gICAgICAgICAgICAvLyAkY3RybC5zdGFuZGFyZEl0ZW1zLnB1c2goe1xyXG4gICAgICAgICAgICAvLyAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgICAvLyAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICAvLyAgIGNvbG9yOiBfcmFuZG9tQ29sb3IoKSxcclxuICAgICAgICAgICAgLy8gICB1c2VyU2V0dGluZ3NPbjogZmFsc2VcclxuICAgICAgICAgICAgLy8gfSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgJGN0cmwub3BlbkRldGFpbCA9IGZ1bmN0aW9uKGl0ZW0pIHtcclxuICAgICAgICAgICAgJGN0cmwuc3RhbmRhcmRJdGVtcy5mb3JFYWNoKGZ1bmN0aW9uKGksIHgpIHtcclxuICAgICAgICAgICAgICAgIGlmIChpdGVtLmNvbG9yICE9PSBpLmNvbG9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaS51c2VyU2V0dGluZ3NPbiA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIGkuc2l6ZVkgPSAyO1xyXG4gICAgICAgICAgICAgICAgICAgIGkuc2l6ZVggPSAzO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGl0ZW0uc2l6ZVggPT0gNCA/IGl0ZW0uc2l6ZVggPSAyIDogaXRlbS5zaXplWCA9IDQ7XHJcbiAgICAgICAgICAgIGl0ZW0uc2l6ZVkgPT0gMiA/IGl0ZW0uc2l6ZVkgPSAxIDogaXRlbS5zaXplWSA9IDI7XHJcbiAgICAgICAgICAgIGl0ZW0udXNlclNldHRpbmdzT24gPT0gdHJ1ZSA/IGl0ZW0udXNlclNldHRpbmdzT24gPSBmYWxzZSA6IGl0ZW0udXNlclNldHRpbmdzT24gPSB0cnVlO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGZ1bmN0aW9uIF9yYW5kb21Db2xvcigpIHtcclxuICAgICAgICAgICAgdmFyIGMgPSBcIiNcIjtcclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCA2OyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGMgKz0gKE1hdGgucmFuZG9tKCkgKiAxNiB8IDApLnRvU3RyaW5nKDE2KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gYztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICRjdHJsLiRwb3N0TGluayA9IGZ1bmN0aW9uKCkge307XHJcblxyXG5cclxuICAgICAgICAkY3RybC5zb3J0SXRlbXMgPSBmdW5jdGlvbihpZCkge1xyXG4gICAgICAgICAgICAkY3RybC5zdGFuZGFyZEl0ZW1zLmZvckVhY2goZnVuY3Rpb24oaSwgeCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGkuaWQgIT0gaWQpIHtcclxuICAgICAgICAgICAgICAgICAgdmFyIGxSb3cgPSBwYXJzZUludChpLnJvdykgKzE7XHJcbiAgICAgICAgICAgICAgICAgICAgaS5yb3cgPSBsUm93LnRvU3RyaW5nKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc29sZS5sb2coaSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH07XHJcbiAgICB9XHJcbn0pKCk7Il0sImZpbGUiOiJjb21wb25lbnRzL3Jlc291cmNlcy9yZXNvdXJjZXMtY29udGFpbmVyLmNvbXBvbmVudC5qcyJ9

(function() {

  'use strict';

  angular
    .module('pProject')

    .component('skillsComponent', {
      templateUrl: 'pproject/components/skills/skills.component.html',
      controller: ['$scope', 'toasty', SkillsComponentController],
      bindings: {}
    });

  function SkillsComponentController($scope, toasty) {
    // SkillsComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'SkillsComponentController:',
        msg: 'started!'
      });
    };
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21wb25lbnRzL3NraWxscy9za2lsbHMuY29tcG9uZW50LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbigpIHtcclxuXHJcbiAgJ3VzZSBzdHJpY3QnO1xyXG5cclxuICBhbmd1bGFyXHJcbiAgICAubW9kdWxlKCdwUHJvamVjdCcpXHJcblxyXG4gICAgLmNvbXBvbmVudCgnc2tpbGxzQ29tcG9uZW50Jywge1xyXG4gICAgICB0ZW1wbGF0ZVVybDogJ3Bwcm9qZWN0L2NvbXBvbmVudHMvc2tpbGxzL3NraWxscy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICAgIGNvbnRyb2xsZXI6IFsnJHNjb3BlJywgJ3RvYXN0eScsIFNraWxsc0NvbXBvbmVudENvbnRyb2xsZXJdLFxyXG4gICAgICBiaW5kaW5nczoge31cclxuICAgIH0pO1xyXG5cclxuICBmdW5jdGlvbiBTa2lsbHNDb21wb25lbnRDb250cm9sbGVyKCRzY29wZSwgdG9hc3R5KSB7XHJcbiAgICAvLyBTa2lsbHNDb21wb25lbnRDb250cm9sbGVyIGZvciBQcmlvcml0eVByb2plY3RBcHAgY29tcG9uZW50XHJcbiAgICB2YXIgJGN0cmwgPSB0aGlzO1xyXG5cclxuICAgICRjdHJsLiRvbkluaXQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdG9hc3R5LnN1Y2Nlc3Moe1xyXG4gICAgICAgIHRpbWVvdXQ6IDM1MDAsXHJcbiAgICAgICAgdGl0bGU6ICdTa2lsbHNDb21wb25lbnRDb250cm9sbGVyOicsXHJcbiAgICAgICAgbXNnOiAnc3RhcnRlZCEnXHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuICB9XHJcbn0pKCk7XHJcbiJdLCJmaWxlIjoiY29tcG9uZW50cy9za2lsbHMvc2tpbGxzLmNvbXBvbmVudC5qcyJ9

(function() {

  'use strict';

  angular
    .module('pProject')
    .directive('menuButtons', menuButtons);

    menuButtons.$inject = ['MenuService', '$timeout'];

  function menuButtons(MenuService, $timeout) {
    return {
      restrict: 'EA',
      templateUrl: '/pproject/directives/menu-buttons/menu.buttons.html',
      scope: {
        items: '<'
      },
      link: function(scope, element, attrs, ctrls) {

        scope.setActive = function(item) {
          scope.items.forEach(function(i, x) {
            item.active = true;
            $timeout(function() {
              MenuService.closeMenu();
            }, 200);
            if (i.label !== item.label) {
              i.active = false;
            }
          });

        }
      }
    }
  }
})();

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL21lbnUtYnV0dG9ucy9tZW51LmJ1dHRvbnMuanMiXSwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uKCkge1xyXG5cclxuICAndXNlIHN0cmljdCc7XHJcblxyXG4gIGFuZ3VsYXJcclxuICAgIC5tb2R1bGUoJ3BQcm9qZWN0JylcclxuICAgIC5kaXJlY3RpdmUoJ21lbnVCdXR0b25zJywgbWVudUJ1dHRvbnMpO1xyXG5cclxuICAgIG1lbnVCdXR0b25zLiRpbmplY3QgPSBbJ01lbnVTZXJ2aWNlJywgJyR0aW1lb3V0J107XHJcblxyXG4gIGZ1bmN0aW9uIG1lbnVCdXR0b25zKE1lbnVTZXJ2aWNlLCAkdGltZW91dCkge1xyXG4gICAgcmV0dXJuIHtcclxuICAgICAgcmVzdHJpY3Q6ICdFQScsXHJcbiAgICAgIHRlbXBsYXRlVXJsOiAnL3Bwcm9qZWN0L2RpcmVjdGl2ZXMvbWVudS1idXR0b25zL21lbnUuYnV0dG9ucy5odG1sJyxcclxuICAgICAgc2NvcGU6IHtcclxuICAgICAgICBpdGVtczogJzwnXHJcbiAgICAgIH0sXHJcbiAgICAgIGxpbms6IGZ1bmN0aW9uKHNjb3BlLCBlbGVtZW50LCBhdHRycywgY3RybHMpIHtcclxuXHJcbiAgICAgICAgc2NvcGUuc2V0QWN0aXZlID0gZnVuY3Rpb24oaXRlbSkge1xyXG4gICAgICAgICAgc2NvcGUuaXRlbXMuZm9yRWFjaChmdW5jdGlvbihpLCB4KSB7XHJcbiAgICAgICAgICAgIGl0ZW0uYWN0aXZlID0gdHJ1ZTtcclxuICAgICAgICAgICAgJHRpbWVvdXQoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgTWVudVNlcnZpY2UuY2xvc2VNZW51KCk7XHJcbiAgICAgICAgICAgIH0sIDIwMCk7XHJcbiAgICAgICAgICAgIGlmIChpLmxhYmVsICE9PSBpdGVtLmxhYmVsKSB7XHJcbiAgICAgICAgICAgICAgaS5hY3RpdmUgPSBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxufSkoKTtcclxuIl0sImZpbGUiOiJkaXJlY3RpdmVzL21lbnUtYnV0dG9ucy9tZW51LmJ1dHRvbnMuanMifQ==
