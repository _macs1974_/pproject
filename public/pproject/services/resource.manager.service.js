(function() {

  "use strict";

  angular
    .module("pProject")
    .factory("ResourceManager", ResourceManager);

  ResourceManager.$inject = ['$q', '$timeout', '$http', 'ResourceModel', 'SkillModel', 'LevelModel'];

  function ResourceManager($q, $timeout, $http, ResourceModel, SkillModel, LevelModel) {
    // factory ResourceManager for module pproject
    // create variable

    var resourceManager = {
      _pool: {},
      _retrieveInstance: function(resourceId, resourceData) {
        var instance = this._pool[resourceId];

        if (instance) {
          instance.setData(resourceData);
        } else {
          instance = new Order(resourceData);
          this._pool[resourceId] = instance;
        }
        return instance;
      },
      _search: function(resourceId) {
        return this._pool[resourceId];
      },
      _load: function(resourceId, deferred) {
        var scope = this;

        $http.get('/api/priority/get/' + resourceId)
          .success(function(resourceData) {
            var resource = scope._retrieveInstance(resourceData.id, resourceData);
            deferred.resolve(resource);
          })
          .error(function() {
            deferred.reject();
          });
      },
      /* Public Methods */
      /* Use this function in order to get a book instance by it's id */
      getResource: function(resourceId) {
        var deferred = $q.defer();
        var resource = this._search(resourceId);
        if (resource) {
          deferred.resolve(resource);
        } else {
          this._load(resourceId, deferred);
        }
        return deferred.promise;
      },
      /* Use this function in order to get instances of all the books */
      loadAllResources: function() {
        var deferred = $q.defer();
        var scope = this;
        $http.get('/api/priority/list')
          .then(function(resourcesArray) {
            var resources = [];
            resourcesArray.data.forEach(function(resourceData) {
              var resource = scope._retrieveInstance(resourceData.id, resourceData);
              resources.push(resource);
            });
            deferred.resolve(resources);
          })
          .catch(function(error) {
            deferred.reject(error);
          });
        return deferred.promise;
      },
      /*  This function is useful when we got somehow the book data and we wish to store it or update the pool and get a book instance in return */
      setResource: function(resourceData) {
        var scope = this;
        var resource = this._search(resourceData.id);
        if (resource) {
          resource.setData(resourceData);
        } else {
          resource = scope._retrieveInstance(resourceData);
        }
        return resource;
      }
    };
    return resourceManager;
  }
})();
