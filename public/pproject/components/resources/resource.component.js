(function() {

    'use strict';

    angular
        .module('pProject')

    .component('resourceComponent', {
        templateUrl: 'pproject/components/resources/resource.component.html',
        controller: ['$scope', 'toasty', '$timeout', ResourceComponentController],
        bindings: {
            userItem: '<',
            sortItemsCb: '&'
        }
    });

    function ResourceComponentController($scope, toasty, $timeout) {
        // ResourcesComponentController for PriorityProjectApp component
        var $ctrl = this;

        $ctrl.$onInit = function() {
            toasty.success({
                timeout: 3500,
                title: 'ResourceComponentController:',
                msg: 'started! ' + $ctrl.userItem.color
            });
        };

        $ctrl.addResource = function() {};

        $ctrl.showBackCard = function(item) {
            $ctrl.sortItemsCb({
                id: item.id
            });

            item.showBack = !item.showBack;
            if (item.showBack) {
                //item.sizeX = 6;
                // item.sizeY = 2;
            } else {
                //item.sizeX = 3;
                // item.sizeY = 2;
            }
        };

        $ctrl.$postLink = function() {};
    }
})();