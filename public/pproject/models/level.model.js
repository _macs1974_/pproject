(function() {

  'use strict';

  angular
    .module("pProject")
    .factory("LevelModel", LevelModel);

  LevelModel.$inject = ["$http"];

  function LevelModel($http) {

    var Level = function(data) {

      this.levelID = data.levelID;
      this.description = data.description;

    };

    Level.build = function(data) {
      return new Skill(data);
    };

    Level.save = function() {
      if (this.levelID) {
        return $http.put(APIURL, this.data);
      }
    };

    return Level;
  }
})();
