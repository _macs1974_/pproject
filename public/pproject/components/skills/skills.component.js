(function() {

  'use strict';

  angular
    .module('pProject')

    .component('skillsComponent', {
      templateUrl: 'pproject/components/skills/skills.component.html',
      controller: ['$scope', 'toasty', SkillsComponentController],
      bindings: {}
    });

  function SkillsComponentController($scope, toasty) {
    // SkillsComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'SkillsComponentController:',
        msg: 'started!'
      });
    };
  }
})();
