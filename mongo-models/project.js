const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const projectSchema = new Schema({
    _id: Schema.Types.ObjectId,
    name: String,
    section: String,
    phase: [{
        type: Schema.Types.ObjectId,
        ref: 'Phase'
    }],
    delivery: Date
});
const Project = mongoose.model('Project', projectSchema);

module.exports = Project;