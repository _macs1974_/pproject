(function() {

    'use strict';

    angular
        .module('pProject')
        .component('resourcesContainerComponent', {
            templateUrl: 'pproject/components/resources/resources-container.component.html',
            controller: ['$scope', 'toasty', 'GridsterSetOpt', ResourcesContainerComponentController],
            bindings: {}
        });

    function ResourcesContainerComponentController($scope, toasty, GridsterSetOpt) {
        // ResourcesComponentController for PriorityProjectApp component

        var $ctrl = this;
        $ctrl.standardItems = [];

        $ctrl.$onInit = function() {

            for (var i = 0; i < 4; i++) {
                $ctrl.standardItems.push({
                    id: i,
                    sizeX: 3,
                    sizeY: 2,
                    color: _randomColor(),
                    showBack: false,
                    userSettingsOn: false
                });
            }

            $ctrl.gridsterOpt = GridsterSetOpt.getOpt();

            // toasty.success({
            //   timeout: 3500,
            //   title: 'ResourcesComponentController:',
            //   msg: 'started!'
            // });
        };

        $ctrl.addResource = function() {
            $ctrl.addUser = !$ctrl.addUser;
            // $ctrl.standardItems.push({
            //   sizeX: 2,
            //   sizeY: 1,
            //   color: _randomColor(),
            //   userSettingsOn: false
            // });
        };

        $ctrl.openDetail = function(item) {
            $ctrl.standardItems.forEach(function(i, x) {
                if (item.color !== i.color) {
                    i.userSettingsOn = false;
                    i.sizeY = 2;
                    i.sizeX = 3;
                }
            });

            item.sizeX == 4 ? item.sizeX = 2 : item.sizeX = 4;
            item.sizeY == 2 ? item.sizeY = 1 : item.sizeY = 2;
            item.userSettingsOn == true ? item.userSettingsOn = false : item.userSettingsOn = true;
        };

        function _randomColor() {
            var c = "#";
            for (var i = 0; i < 6; i++) {
                c += (Math.random() * 16 | 0).toString(16);
            }
            return c;
        }

        $ctrl.$postLink = function() {};


        $ctrl.sortItems = function(id) {
            $ctrl.standardItems.forEach(function(i, x) {
                if (i.id != id) {
                  var lRow = parseInt(i.row) +1;
                    i.row = lRow.toString();
                    console.log(i);
                }
            });
        };
    }
})();