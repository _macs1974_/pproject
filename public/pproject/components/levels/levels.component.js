(function() {

  'use strict';

  angular
    .module('pProject')

    .component('levelsComponent', {
      templateUrl: 'pproject/components/levels/levels.component.html',
      controller: ['$scope', 'toasty', LevelsComponentController],
      bindings: {}
    });

  function LevelsComponentController($scope, toasty) {
    // LevelsComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'LevelsComponentController:',
        msg: 'started!'
      });
    };
  }
})();
