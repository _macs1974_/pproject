const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const phaseSchema = new Schema({
    _id: Schema.Types.ObjectId,
    code: Number,
    description: String
});
const Phase = mongoose.model('Phase', phaseSchema);

module.exports = Phase;