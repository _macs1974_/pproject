(function() {

  'use strict';

  angular
    .module('pProject')
    .directive('menuButtons', menuButtons);

    menuButtons.$inject = ['MenuService', '$timeout'];

  function menuButtons(MenuService, $timeout) {
    return {
      restrict: 'EA',
      templateUrl: '/pproject/directives/menu-buttons/menu.buttons.html',
      scope: {
        items: '<'
      },
      link: function(scope, element, attrs, ctrls) {

        scope.setActive = function(item) {
          scope.items.forEach(function(i, x) {
            item.active = true;
            $timeout(function() {
              MenuService.closeMenu();
            }, 200);
            if (i.label !== item.label) {
              i.active = false;
            }
          });

        }
      }
    }
  }
})();
