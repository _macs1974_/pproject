(function() {

  'use strict';

  angular
    .module('pProject')

    .component('indexComponent', {
      templateUrl: 'pproject/components/index/index.component.html',
      controller: ['$scope', 'toasty', IndexComponentController],
      bindings: {}
    });

  function IndexComponentController($scope, toasty) {
    // IndexComponentController for PriorityProjectApp component
    var $ctrl = this;

    $ctrl.$onInit = function() {
      toasty.success({
        timeout: 3500,
        title: 'IndexComponentController:',
        msg: 'started!'
      });
    };
  }
})();
