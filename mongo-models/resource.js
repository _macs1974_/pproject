const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const resourceSchema = new Schema({
    _id: Schema.Types.ObjectId,
    name: String,
    surname: String,
    level: String,
    competences: [{
        type: Schema.Types.ObjectId,
        ref: 'Competence'
    }]
});
const Resource = mongoose.model('Resource', resourceSchema);

module.exports = Resource;