var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    Project = require('../mongo-models/project');

/*
{
    _id: Schema.Types.ObjectId,
    name: String,
    section: String,
    phase: [{
        type: Schema.Types.ObjectId,
        ref: 'Phase'
    }],
    delivery: Date
}
*/

router.get('/project', function(req, res, next) {
    Project.find({}, function(err, projectList) {
        if (err) res.send(err);
        res.json(projectList);
    });
});

router.put('/project', function(req, res, next) {
	/*var project = new Project({
        name: 'Macs S.R.L.',
        section: 'Macs S.R.L. costruzioni meccaniche',
        phase: {
            email: 'franchi.massimiliano@gmail.com',
            telefono: '+393382982119',
            sito: 'www.macs-srl.com'
        },
        delivery: new Date()
    });
    macs.save(function(err) {
        if (err) throw err;
        console.log('Customer saved successfully!');
    });*/
});

router.post('/project', function(req, res, next) {
});

router.delete('/project', function(req, res, next) {
});

module.exports = router;